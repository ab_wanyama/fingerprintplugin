// Empty constructor
function Fingerprint() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
Fingerprint.prototype.show = function(message, duration, successCallback, errorCallback) {
  var options = {};
  options.message = message;
  options.duration = duration;
  cordova.exec(successCallback, errorCallback, 'Fingerprint', 'show', [options]);
}

// Installation constructor that binds ToastyPlugin to window
Fingerprint.install = function() {
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.fingerprintPlugin = new Fingerprint();
  return window.plugins.fingerprintPlugin;
};

cordova.addConstructor(Fingerprint.install);